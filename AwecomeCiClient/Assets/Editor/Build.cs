﻿using UnityEditor;
using UnityEditor.Build.Reporting;

public class Build
{
    public static void BuildAndroid()
    {
        string[] levels = {"Assets/Scenes/SampleScene.unity"};

        BuildReport report = BuildPipeline.BuildPlayer(levels, "_android/", BuildTarget.Android, BuildOptions.AcceptExternalModificationsToPlayer);
        EditorApplication.Exit(report.summary.result == BuildResult.Succeeded ? 0 : 1);
    }
}
